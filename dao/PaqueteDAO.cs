﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using tour_huaraz_ws_v2.model;
using tour_huaraz_ws_v2.wrapper;

namespace tour_huaraz_ws_v2.dao
{
    public class PaqueteDAO
    {
        public static IList<Paquete> Listar()
        {
            string query = "select" +
                "  a.codigo" +
                ", a.nombre nombre_lugar" +
                ", a.tipo" +
                ", b.codigo_paquete" +
                ", c.nombre nombre_paquete" +
                ", c.tarifa_a" +
                ", c.tarifa_b" +
                ", c.tarifa_c" +
                ", c.estado " +
                "from lugar a " +
                "inner join detalle_paquete b on a.codigo = b.codigo_lugar " +
                "inner join paquete c on b.codigo_paquete = c.codigo " +
                "where b.estado = 'Activo' " +
                "order by c.codigo, a.nombre";

            DataTable table = Datasource.ExecuteQuery(query, null);
            IList<Paquete> paquetes = new List<Paquete>();
            string codigoPaquete = "-1";
            string codigoPaqueteTemp;
            Paquete paquete = new Paquete();
            foreach (DataRow row in table.Rows)
            {
                codigoPaqueteTemp = row["codigo_paquete"].ToString();
                if (codigoPaquete != codigoPaqueteTemp)
                {
                    codigoPaquete = codigoPaqueteTemp;
                    paquete = new PaqueteWrapper(row).Paquete;
                    paquetes.Add(paquete);
                }

                paquete.Lugares.Add(new LugarWrapper(row).Lugar);
            }

            return paquetes;
        }
    }
}
