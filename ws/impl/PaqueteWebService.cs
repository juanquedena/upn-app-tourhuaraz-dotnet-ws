﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tour_huaraz_ws_v2.dao;
using tour_huaraz_ws_v2.model;

namespace tour_huaraz_ws_v2.ws.impl
{
    public class PaqueteWebService : IPaqueteWebService
    {
        public IList<Paquete> ListarPaquetes()
        {
            return PaqueteDAO.Listar();
        }
    }
}
