﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using tour_huaraz_ws_v2.model;

namespace tour_huaraz_ws_v2.ws
{
    [ServiceContract]
    public interface IPaqueteWebService
    {

        [OperationContract]
        IList<Paquete> ListarPaquetes();
    }
}
