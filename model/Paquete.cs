﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace tour_huaraz_ws_v2.model
{
    [DataContract]
    public class Paquete
    {
        public int? PaqueteID { get; set; }
        public string PaqueteName { get; set; }

        public double? TarifaA { get; set; }

        public double? TarifaB { get; set; }

        public double? TarifaC { get; set; }

        public List<Lugar> Lugares { get; set; }
    }
}