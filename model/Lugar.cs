﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace tour_huaraz_ws_v2.model
{

    [DataContract]
    public class Lugar
    {
        public int? LugarID { get; set; }

        public string LugarName { get; set; }

        public string LugarType { get; set; }
    }
}