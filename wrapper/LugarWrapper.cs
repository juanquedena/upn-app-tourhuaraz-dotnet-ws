﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using tour_huaraz_ws_v2.Db.Wrapper;
using tour_huaraz_ws_v2.model;

namespace tour_huaraz_ws_v2.wrapper
{
    public class LugarWrapper : AbstractWrapper
    {

        public Lugar Lugar { get; }

        public LugarWrapper(DataRow dataRow)
        {
            Row = dataRow;
            Lugar = new Lugar
            {
                LugarID = FieldInt("codigo"),
                LugarName = FieldString("nombre_lugar"),
                LugarType = FieldString("tipo")
            };
        }
    }
}
