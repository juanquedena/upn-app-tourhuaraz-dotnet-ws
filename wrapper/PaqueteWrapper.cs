﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using tour_huaraz_ws_v2.Db.Wrapper;
using tour_huaraz_ws_v2.model;

namespace tour_huaraz_ws_v2.wrapper
{
    public class PaqueteWrapper : AbstractWrapper
    {
        
        public Paquete Paquete { get; }

        public PaqueteWrapper(DataRow dataRow)
        {
            Row = dataRow;
            Paquete = new Paquete
            {
                PaqueteID = FieldInt("codigo_paquete"),
                PaqueteName = FieldString("nombre_paquete"),
                TarifaA = FieldDouble("tarifa_a"),
                TarifaB = FieldDouble("tarifa_b"),
                TarifaC = FieldDouble("tarifa_c"),
                Lugares = new List<Lugar>()
            };
        }
    }
}
