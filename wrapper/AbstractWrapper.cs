﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;

namespace tour_huaraz_ws_v2.Db.Wrapper
{
    public class AbstractWrapper
    {

        protected DataRow Row;

        protected String FieldString(string fieldName)
        {
            return Row[fieldName]?.ToString();
        }

        protected int? FieldInt(string fieldName)
        {
            String value = FieldString(fieldName);
            int? intValue = null;
            intValue = value.Length > 0 ? int.Parse(value.ToString()) : intValue;
            return intValue;
        }

        protected double? FieldDouble(string fieldName)
        {
            String value = FieldString(fieldName);
            double? intValue = null;
            intValue = value.Length > 0 ? double.Parse(value.ToString()) : intValue;
            return intValue;
        }

        protected bool FieldBool(string fieldName)
        {
            String value = FieldString(fieldName);
            bool boolValue = false;
            boolValue = value.Length > 0 ? bool.Parse(value.ToString()) : boolValue;
            return boolValue;
        }
    }
}
